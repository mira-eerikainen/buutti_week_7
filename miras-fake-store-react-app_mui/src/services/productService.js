const API = "https://fakestoreapi.com/products";

export const fetchAllProducts = async () => {
  const res = await fetch(API);
  const data = await res.json();
  return data;
};

export const fetchCategories = async () => {
  const res = await fetch(`${API}/categories`);
  const data = await res.json();
  console.log(data);
  return data;
};

export const fetchOneProduct = async (id) => {
  const res = await fetch(`${API}/${id}`);
  const data = await res.json();
  return data;
};

export const deleteProduct = async (id) => {
  const res = await fetch(`${API}/${id}`, {
    method: "DELETE"
  });
  console.log(`DELETE fired on ID: ${id}`);
  const data = await res.json();
  return data;
};

export const updateProduct = async (id) => {
  const res = await fetch(`${API}/${id}`, {
    method:"POST",
    body:JSON.stringify(
      {
        title: "test product",
        price: 13.5,
        description: "lorem ipsum set",
        image: "https://i.pravatar.cc",
        category: "electronic"
      }
    )
  });
  console.log(`PUT fired on ID: ${id}`);
  const data = await res.json();
  return data;
};



