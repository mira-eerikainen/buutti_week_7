import { useState, createContext } from "react";

export const AppContext = createContext([{}, () => {}]);

export const ContextProvider = (props) => {
  const initialContext = {
    product: {
      id: 0,
      title: "",
      description: "",
      image: ""
    }
  };
  const [state, setState] = useState(initialContext);
  return (
    <AppContext.Provider value={[state, setState]}>
      {props.children}
    </AppContext.Provider>
  );
};