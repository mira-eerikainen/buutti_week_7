import { Container } from "@mui/material";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { fetchOneProduct } from "../services/productService";
import { ProductCard } from "./ProductCard";

// const API = "https://fakestoreapi.com/products";

export const OneProduct = () => {
  const params = useParams();

  const [item, setItem] = useState([]);

  useEffect(() => {
    const getOneProduct = async () => {
      const data = await fetchOneProduct(params.id);
      setItem(data);
    };
    getOneProduct();
  },[params.id]);

  // const deleteProduct = async (id) => {
  //   await fetch(`${API}/${id}`, {
  //     method: "DELETE"
  //   });
  //   console.log(`DELETE fired on ID: ${id}`);
  // };

  return (
    <Container
      maxWidth="xs">
      <ProductCard 
        item={item} 
        // deleteProduct={deleteProduct} 
      />
    </Container>
  );
};
