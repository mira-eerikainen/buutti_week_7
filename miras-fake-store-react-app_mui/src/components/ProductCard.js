import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
// import IconButton from "@mui/material/IconButton";
// import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";
// import UpdateIcon from "@mui/icons-material/Update";
import { Box, Button } from "@mui/material";
// import { CardActions } from "@mui/material";
import { CardMedia } from "@mui/material";
// import { useNavigate } from "react-router-dom";

// const API = "https://fakestoreapi.com/products";

export const ProductCard = ({ item, deleteProduct }) => {
  // const navigate = useNavigate();

  // const handleClick = (e, id) => {
  //   e.preventDefault();
  //   navigate(`/${id}`);
  // };

  // const pathname = window.location.pathname;

  return (
    <Box>      
      <Card 
        sx={{
          p: "24px"
        }}
        elevation={1}
      >
        <CardHeader
          title={item.title}
          sx={{
            mb: "42px"
          }}
        />
        {/* <CardActions onClick={e => handleClick(e, item.id)}> */}
        <CardMedia
          component="img"
          image={item.image}
          alt="random"
          cursor="pointer"
        />
        {/* </CardActions> */}
        <CardContent>
          <Typography variant="body2" color="textSecondary">
            { item.description }
          </Typography>
        </CardContent>
        <Button onClick={() => deleteProduct(item.id)}>Delete</Button>
        {/* {pathname === `/${item.id}` ?
          <CardActions>
            <Button onClick={() => deleteProduct(item.id)}>Delete</Button>
          </CardActions>
          : null} */}
      </Card>
    </Box>
  );
};
