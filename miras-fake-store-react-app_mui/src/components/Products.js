import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { fetchAllProducts, fetchCategories, deleteProduct } from "../services/productService";
import Container from "@mui/material/Container";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { AppBar, Button, Card, CardContent, CardHeader, CardMedia, CssBaseline, Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";

export const Products = () => {
  const [products, setProducts] = useState([]);
  const [categories, setFiltered] = useState([]);

  useEffect(() =>{
    const getProducts = async () => {
      const data = await fetchAllProducts();
      setProducts(data);
    };
    getProducts();
  },[]);

  useEffect(() =>{
    const getGategories = async () => {
      const data = await fetchCategories();
      setFiltered(data);
    };
    getGategories();
  },[]);

  const navigate = useNavigate();

  const handleSelect = (event) => {
    event.preventDefault();
    const path = event.target.value;
    const cleanPath = path.replace("'", "").replace(" ", "");
    navigate(cleanPath);
  };

  const handleDelete = async (e, id) => {
    e.preventDefault();
    await deleteProduct(id);
    setProducts((products) => {
      return products.filter(item => item.id !== id);
    });
  };

  return (
    <Box>
      <CssBaseline />
      <Container 
        maxWidth={false}
        disableGutters={true}
      >
        <AppBar position="relative">
          <Typography
            p="20px"
            backgroundColor="inherit"
            color="inherit"
            component="h1"
            variant="h3"
            align="center"
          >
        Mira&apos;s Fake Store
          </Typography>
        </AppBar>
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <FormControl style={{ minWidth: "250px", marginTop:"40px"}}>
            <InputLabel id="category-select-label">Category</InputLabel>
            <Select
              labelId="category-select-label"
              id="categories"
              value={categories}
              defaultValue={"All Products"}
              label="Category"
              onChange={e => handleSelect(e)}
            >{categories.map((category) => {
                return (
                  <MenuItem 
                    value={category}
                    defaultValue={"All Products"}
                    key={category}
                    id={category}
                  >{category}</MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </Box>
        <Box
          style={{ display: "flex", justifyContent: "center" }}
        >
          <Grid 
            container spacing={6} 
            maxWidth="lg"
          >
            {products.map(item => (
              <Grid 
                item key={item.id} xs={12} sm={6} md={4}
                style={{display: "flex", marginTop: "40px"}}
              >
                <Card 
                  style={{display: "flex", justifyContent: "space-between", flexDirection: "column", padding: "24px"}}
                  elevation={1}
                >
                  <CardHeader
                    title={item.title}
                    sx={{ mb: "42px" }}
                  />
                  {/* <CardActions onClick={e => handleClick(e, item.id)}> */}
                  <CardMedia
                    component="img"
                    image={item.image}
                    alt="random"
                    cursor="pointer"
                  />
                  {/* </CardActions> */}
                  <CardContent>
                    <Typography variant="body2" color="textSecondary">
                      { item.description }
                    </Typography>
                  </CardContent>
                  <Button variant="contained" onClick={(e) => handleDelete(e, item.id)}>Delete</Button>
                  {/* {pathname === `/${item.id}` ?
          <CardActions>
            <Button onClick={() => deleteProduct(item.id)}>Delete</Button>
          </CardActions>
          : null} */}
                </Card>
              </Grid>
            ))}
          </Grid>
        </Box>
      </Container>
    </Box>
  );
};
