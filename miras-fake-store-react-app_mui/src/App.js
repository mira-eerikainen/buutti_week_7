import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { OneProduct } from "./components/OneProduct";
import { Products } from "./components/Products";
import NotFound from "./components/NotFound.js";

const App = () => {
  
  return (
    <div className="container">
      <Router>
        <Routes>
          <Route 
            exact path="/"
            element={<Products />}>
          </Route>
          <Route 
            exact path="/:id"
            element={<OneProduct />}>
          </Route>
          <Route path="*"
            element={<NotFound />}></Route> 
        </Routes>
      </Router>
    </div>
  );
};

export default App;

