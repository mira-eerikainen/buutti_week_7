import React, {useState} from "react";
import { v4 as uuidv4 } from "uuid";

function TodoForm(props) {
  const [input, setInput] = useState("");

  const handleChange = e => {
    setInput(e.target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();

    props.onSubmit({
      id: uuidv4(),
      text: input
    });

    setInput("");
  };

  return(

    <form className="todo-form" onSubmit={handleSubmit}>
      <input 
        className="input" 
        onChange={handleChange} 
        type="text" 
        placeholder="Add a todo" 
        value={input} 
        name="text" />
      <button 
        className="btn" 
        type="submit">
          Add
      </button>
    </form>
  );
}

export default TodoForm;
