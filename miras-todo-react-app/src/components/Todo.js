import React, { useState } from "react";
// import TodoForm from "./TodoForm";
// import TodoList from "./TodoList";
import { MdDone } from "react-icons/md";
import { TiDelete } from "react-icons/ti";

function Todo({todos, completeTodo, deleteTodo}) {
  return todos.map((todo, index) => (
    <div 
      className={todo.isDone ? "todo-row done" : "todo-row"} 
      key={index}>
      <div 
        key={todo.id} onClick={() => completeTodo(todo.id)}>
        {todo.text}
      </div>
      <div className="icons">
        <MdDone className="icons done-icon" onClick={() => completeTodo(todo.id)}/>
        <TiDelete className="icons dlt-icon" onClick={() => deleteTodo(todo.id)}/>
      </div>
    </div>
  ));
}

export default Todo;
